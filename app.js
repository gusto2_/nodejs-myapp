var debug = require('debug')('myapp');

var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var users = require('./routes/users');
var personWS = require('./routes/person');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
// HTML5 server routing
app.use('/detail', function(req, res){
   res.sendfile('./public/yuitest07.html');
});
app.use('/users', users);
app.use('/person', personWS);
app.use('/person/:id', personWS);

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


// There are many useful environment variables available in process.env.
// // VCAP_APPLICATION contains useful information about a deployed application.
 var appInfo = JSON.parse(process.env.VCAP_APPLICATION || "{}");
// // TODO: Get application information and use it in your app.
//
// // VCAP_SERVICES contains all the credentials of services bound to
// // this application. For details of its content, please refer to
// // the document or sample of each service.
 var services = JSON.parse(process.env.VCAP_SERVICES || "{}");
 console.log('app.js services: '+services);
// // TODO: Get service credentials and communicate with bluemix services.
//
// // The IP address of the Cloud Foundry DEA (Droplet Execution Agent) that hosts this application:
 var host = (process.env.VCAP_APP_HOST || '0.0.0.0');
// // The port on the DEA for communication with the application:
 var port = (process.env.VCAP_APP_PORT || 8181);
// // Start server
 var server = app.listen(port, host, function() {
    console.log('Express server listening on port ' + server.address().port);
    debug('Express server listening on port ' + server.address().port);
     });
 console.log('App started on port ' + port);


//module.exports = app;
