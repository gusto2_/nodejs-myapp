var localDatabaseURI = "localhost:27017/test";

var services = JSON.parse(process.env.VCAP_SERVICES || "{}");
console.log('db.js services: '+services);

var databaseURI;
if(!services.mongolab) {
	databaseURI = localDatabaseURI;
}
else {
	
	databaseURI = services.mongolab[0].credentials.uri;
}

var collections = ["person"];
var db = require("mongojs").connect(databaseURI, collections);
var ObjectID = require('mongodb').ObjectID;

module.exports = db;

