
var express = require('express');
var router = express.Router();
var db = require('../db');
var url = require('url');

var ObjectID = require('mongodb').ObjectID;


/*
 * list records, optionaly apply a filter with the 'rrn' parameter
 * /person[?rrn=xxxxxx]
 */
router.get('/', function(req, res) {
   res.contentType('application/json;charset=UTF-8');
   var personList = [];
   console.log('listing all records');
   var filter = null;
   if(req.query.rrn!=null) {
        console.log('rrn filter: '+req.query.rrn);
   	filter = {rrn: req.query.rrn};
   }
   db.person.find(filter, function(err,person) {
	   person.forEach(function(p) {
		   console.log('adding person: '+p);
		   if(p._id)
		     p.id = p._id.toString();
		   personList.push(p);
		   });
	   console.log('record count: '+person.length);
	   res.send(personList);
	   });
		});

/**
 * get specific record based on id
 * /person/id
 */
router.get('/:id', function(req, res) {
	res.contentType('application/json;charset=UTF-8');
	console.log('user id param: '+req.params.id);
	db.person.find(
		  {_id: new ObjectID(req.params.id)},
		  function(err,personList) {
		 personList.forEach(function(p) {
			 if(p._id)
				 p.id = p._id.toString();
			 res.send(p);
			 return;
		});
		 if(personList.length==0) {
			res.send("Record not found", 404);
		}
	     } );
	}
); 

/**
 *create a record
 */

router.post('/',function(req,res) {
  res.contentType('application/json;charset=UTF-8');
  var body = req.body;
  if(body.name ==null || body.rrn==null
    || body.name == '' ||  body.rrn== ''  ) {
  	res.send("Mandatory attributes: name, born, rrn", 400);
	return;
  }
  console.log('creating a record :'+body);
  db.person.save(body, function(err, saved) {
	if( err || !saved ) 
	{
	  console.log("User not saved");
	  res.send(err,500);
	}
	  else {
	   console.log("User saved");
	   if(saved._id)
		   saved.id = saved._id.toString();
	   res.send(saved,200);
	  }

  });
});

router.delete('/:id', function(req, res) {
   var id = req.params.id;
   if(id==null)
   {
   	res.send("Not found", 404);
	return;
   }
   var filter = { _id: new ObjectID(id)};
   db.person.remove(filter, function(err,removed) {
	if(err || !removed) {
		console.log('Record not removed');
		res.send(err,500);
	}
	else {
		console.log('Record removed');
		res.send(removed,200);
	}
   });
});

module.exports = router;

